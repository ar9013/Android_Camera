package arsuite.yue.com.icrtandroid;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.hardware.Camera;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * Created by luokangyu on 2017/9/18.
 */

public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback {

    private String TAG = "CameraPreview";
    private SurfaceHolder sholder;
    private Camera camera;
    private Camera.CameraInfo param ;



    public CameraPreview(Context context, Camera camera) {
        super(context);

        this.camera =camera;

        sholder = getHolder();
        sholder.addCallback(this);
        sholder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {

        try{
            camera.setPreviewDisplay(holder);
            camera.setDisplayOrientation(90);  // 設定翻轉 90 預覽
            camera.startPreview();

        }catch (Exception ex){

            Log.d(TAG,"Error setting camera preview: " + ex.getMessage());
        }

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {

        if (sholder.getSurface() == null){
            // preview surface does not exist
            return;
        }


        try{
            camera.stopPreview();
        }catch (Exception ex){

        }

        try{
            camera.setPreviewDisplay(sholder);
            camera.startPreview();
        }catch (Exception ex){
            Log.d(TAG,"Error starting camera preview: " + ex.getMessage());
        }

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {

    }
}
