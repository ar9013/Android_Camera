package arsuite.yue.com.icrtandroid;


import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.hardware.Camera;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;

public class MainActivity extends Activity {

    private String TAG = "MainActivity";
    private Camera camera;
    private CameraPreview cPreview;

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
        System.loadLibrary("opencv_java");
    }

    private boolean checkCameraHardWare(Context context){
        if(context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)){
            return  true;
        }else{
            return false;
        }
    }

    public static Camera getCameraInstance(Context context){
        Camera camera = null;

        try{
            camera = Camera.open();
        }
        catch (Exception ex){
            // Camera is not available (in use or does not exist)
            Toast.makeText(context,"Camera is not available",Toast.LENGTH_SHORT).show();
        }
        return camera;
    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);




        checkCameraHardWare(this);
        camera = getCameraInstance(this);

        cPreview = new CameraPreview(this,camera);
        FrameLayout preview = findViewById(R.id.camera_preview);
        preview.addView(cPreview);


        // Example of a call to a native method
//        TextView tv = (TextView) findViewById(R.id.sample_text);
//        tv.setText(stringFromJNI());
    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public native String stringFromJNI();


}
